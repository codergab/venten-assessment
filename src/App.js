import React from "react";

import InventoryList from "./components/Inventory/InventoryList";
import FilterList from "./components/Filter/FilterList";

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-5">
          <FilterList />
        </div>
        <div className="col-md-7">
          <InventoryList />
        </div>
      </div>
    </div>
  );
}

export default App;
