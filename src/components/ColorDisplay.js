import React from "react";

export default function ColorDisplay(props) {
  return (
    <div
      style={{
        margin: "0.2em .2em 0 0",
        width: "18px",
        height: "18px",
        borderRadius: "50%",
        background: "" + props.color.toString().toLowerCase(),
      }}
    ></div>
  );
}
