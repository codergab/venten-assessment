import React from "react";
import ColorDisplay from "../ColorDisplay";
import "./filter.css";

function FilterItem(props) {
  const { item } = props;

  return (
    <div className="card mb-4 user-select">
      <div className="card-body text-center">
        <h3>
          {item.start_year} - {item.end_year}
        </h3>
        <h6 className="font-weight-bold mt-4">{item.gender}</h6>
        <div className="countries mt-3 mb-2">
          {item.countries.length > 0 &&
            item.countries.map((country, i) => (
              <span key={i} className="badge badge-secondary custom-badge p-2">
                {country}
              </span>
            ))}
        </div>
        <div className="d-flex align-content-center justify-content-center">
          {item.colors.length > 0 &&
            item.colors.map((color, i) => (
              <ColorDisplay key={i} color={color} />
            ))}
        </div>
      </div>
    </div>
  );
}

export default FilterItem;
