import React, { useState, useEffect } from "react";
import FilterItem from "./FilterItem";
import axios from "../../services/axios";
import { FILTER_LIST } from "../../utils/ApiRoutes";

class FilterList extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    filters: [],
  };

  componentDidMount() {
    axios.defaults.baseURL = "https://ven10.co/assessment/filter.json";
    axios.get(FILTER_LIST).then((res) => {
      this.setState({ filters: res.data });
    });
  }

  render() {
    return (
      <React.Fragment>
        <h4 className="mt-2">Filter</h4>
        {this.state.filters.length > 0 && (
          <div>
            {this.state.filters.length > 0 &&
              this.state.filters.map((filter) => (
                <FilterItem key={filter.id} item={filter} />
              ))}
          </div>
        )}
        {this.state.filters.length == 0 && <p>Loading... </p>}
      </React.Fragment>
    );
  }
}

export default FilterList;
