import React from "react";
import "./inventory.css";
import ColorDisplay from "../ColorDisplay";
export default function InventoryItem(props) {
  const inventory = {
    car_name: "Volvo Ruz",
    car_model: "Volvo",
    car_model_year: "2019",
    car_color: "Orange",
    country: "China",
    gender: "Male",
    job: "Mechanical Systems Engineer",
    email: "demo@email.com",
    bio: "loremissf sfnsfnfh fshfbsjf sfbshfsf jfbshsh",
  };
  return (
    <div className="card">
      <div className="card-body">
        <div className="row">
          <div className="col-md-4">
            <img
              className="img-thumbnail"
              src="https://source.unsplash.com/random/200x200"
            />
          </div>
          <div className="col-md-8">
            <h5>Car Name</h5>
            <div className="brand-container mt-2">
              <div className="brand-box pr-2">
                <span className="text-muted">Brand</span> <br />
                <span>{inventory.car_model}</span>
              </div>
              <div className="brand-box pl-2 pr-2">
                <span className="text-muted">Year</span> <br />
                <span>{inventory.car_model_year}</span>
              </div>
              <div className="brand-box pl-2">
                <span className="text-muted">Color</span> <br />
                <ColorDisplay color={inventory.car_color} />
              </div>
            </div>
            <div className="brand-container mt-2">
              <div className="mr-3">
                <span className="text-muted">Country</span>
                <br />
                <span className="font-weight-bold">{inventory.country}</span>
              </div>
              <div className="mr-3">
                <span className="text-muted">Gender</span>
                <br />
                <span className="font-weight-bold">{inventory.gender}</span>
              </div>
              <div className="">
                <span className="text-muted">Job</span>
                <br />
                <span className="font-weight-bold">{inventory.job}</span>
              </div>
            </div>
            <div className="mt-2">
              Email: <strong>dmjfe</strong>
            </div>
            <div>
              Bio: <strong>dmjfe</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
