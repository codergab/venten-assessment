import React, { Component } from "react";
import InventoryItem from "./InventoryItem";

export default class InventoryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inventories: [],
    };
  }
  render() {
    return (
      <React.Fragment>
        <h4 className="mt-2">Inventory List</h4>
        {this.state.inventories.length > 0 &&
          this.state.inventories.map((inventory) => (
            <InventoryItem key={inventory.id} inventory={inventory} />
          ))}
        {this.state.inventories.length == 0 && (
          <div className="alert alert-info">
            Inventories Empty, Please select the filters to fetch inventories
          </div>
        )}
      </React.Fragment>
    );
  }
}
